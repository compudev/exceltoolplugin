# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_exceltoolplugin.ui'
#
# Created: Mon Jun 16 00:49:22 2014
#      by: PyQt4 UI code generator 4.10.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ExcelToolPlugin(object):
    def setupUi(self, ExcelToolPlugin):
        ExcelToolPlugin.setObjectName(_fromUtf8("ExcelToolPlugin"))
        ExcelToolPlugin.resize(421, 200)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/plugins/exceltoolplugin/resources/icon.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        ExcelToolPlugin.setWindowIcon(icon)
        ExcelToolPlugin.setModal(True)
        self.cbxTabla = QtGui.QComboBox(ExcelToolPlugin)
        self.cbxTabla.setGeometry(QtCore.QRect(10, 10, 371, 21))
        self.cbxTabla.setObjectName(_fromUtf8("cbxTabla"))
        self.txtUbicacion = QtGui.QLineEdit(ExcelToolPlugin)
        self.txtUbicacion.setGeometry(QtCore.QRect(10, 60, 371, 21))
        self.txtUbicacion.setObjectName(_fromUtf8("txtUbicacion"))
        self.btnBscUbc = QtGui.QPushButton(ExcelToolPlugin)
        self.btnBscUbc.setGeometry(QtCore.QRect(390, 60, 21, 23))
        self.btnBscUbc.setObjectName(_fromUtf8("btnBscUbc"))
        self.cbxFormat = QtGui.QComboBox(ExcelToolPlugin)
        self.cbxFormat.setGeometry(QtCore.QRect(10, 110, 141, 22))
        self.cbxFormat.setObjectName(_fromUtf8("cbxFormat"))
        self.cbxFormat.addItem(_fromUtf8(""))
        self.cbxFormat.addItem(_fromUtf8(""))
        self.btnOK = QtGui.QPushButton(ExcelToolPlugin)
        self.btnOK.setGeometry(QtCore.QRect(300, 160, 114, 32))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Lucida Grande"))
        font.setBold(True)
        font.setWeight(75)
        self.btnOK.setFont(font)
        self.btnOK.setObjectName(_fromUtf8("btnOK"))
        self.btnCerrar = QtGui.QPushButton(ExcelToolPlugin)
        self.btnCerrar.setGeometry(QtCore.QRect(220, 160, 81, 32))
        self.btnCerrar.setObjectName(_fromUtf8("btnCerrar"))
        self.btnHelp = QtGui.QPushButton(ExcelToolPlugin)
        self.btnHelp.setGeometry(QtCore.QRect(10, 160, 114, 32))
        self.btnHelp.setObjectName(_fromUtf8("btnHelp"))

        self.retranslateUi(ExcelToolPlugin)
        QtCore.QMetaObject.connectSlotsByName(ExcelToolPlugin)

    def retranslateUi(self, ExcelToolPlugin):
        ExcelToolPlugin.setWindowTitle(_translate("ExcelToolPlugin", "Convertir Tabla a Excel", None))
        self.btnBscUbc.setText(_translate("ExcelToolPlugin", "...", None))
        self.cbxFormat.setItemText(0, _translate("ExcelToolPlugin", "Excel", None))
        self.cbxFormat.setItemText(1, _translate("ExcelToolPlugin", "CVS", None))
        self.btnOK.setText(_translate("ExcelToolPlugin", "OK", None))
        self.btnCerrar.setText(_translate("ExcelToolPlugin", "Cerrar", None))
        self.btnHelp.setText(_translate("ExcelToolPlugin", "Ayuda", None))

import resources_rc
