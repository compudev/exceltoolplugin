# -*- coding: utf-8 -*-
__author__ = 'Melquisedec Wilchez <mwilchez@compudev.com.ec>'

from PyQt4 import QtCore, QtGui
from ui_about import Ui_AboutDialog

class AboutDialog(QtGui.QDialog, Ui_AboutDialog):

    def __init__(self):
        QtGui.QDialog.__init__(self)
        self.setupUi(self)