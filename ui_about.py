# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_about.ui'
#
# Created: Sun Jun 15 23:58:47 2014
#      by: PyQt4 UI code generator 4.10.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_AboutDialog(object):
    def setupUi(self, AboutDialog):
        AboutDialog.setObjectName(_fromUtf8("AboutDialog"))
        AboutDialog.resize(513, 418)
        AboutDialog.setModal(True)
        self.buttonBox = QtGui.QDialogButtonBox(AboutDialog)
        self.buttonBox.setGeometry(QtCore.QRect(430, 380, 81, 31))
        self.buttonBox.setOrientation(QtCore.Qt.Vertical)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Close)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.frame = QtGui.QFrame(AboutDialog)
        self.frame.setGeometry(QtCore.QRect(-10, 10, 551, 80))
        self.frame.setStyleSheet(_fromUtf8("background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(0, 0, 0, 0), stop:0.206897 rgba(255, 255, 255, 255));"))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.lbNombre = QtGui.QLabel(self.frame)
        self.lbNombre.setGeometry(QtCore.QRect(30, 20, 341, 41))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Lucida Grande"))
        font.setPointSize(36)
        font.setBold(True)
        font.setWeight(75)
        self.lbNombre.setFont(font)
        self.lbNombre.setObjectName(_fromUtf8("lbNombre"))
        self.lbIcon = QtGui.QLabel(self.frame)
        self.lbIcon.setGeometry(QtCore.QRect(460, 10, 61, 61))
        self.lbIcon.setText(_fromUtf8(""))
        self.lbIcon.setPixmap(QtGui.QPixmap(_fromUtf8(":/plugins/exceltoolplugin/resources/excel-60.png")))
        self.lbIcon.setObjectName(_fromUtf8("lbIcon"))
        self.tabWdgAbout = QtGui.QTabWidget(AboutDialog)
        self.tabWdgAbout.setGeometry(QtCore.QRect(10, 100, 491, 271))
        self.tabWdgAbout.setObjectName(_fromUtf8("tabWdgAbout"))
        self.TbAbout = QtGui.QWidget()
        self.TbAbout.setObjectName(_fromUtf8("TbAbout"))
        self.lbVersion = QtGui.QLabel(self.TbAbout)
        self.lbVersion.setGeometry(QtCore.QRect(10, 10, 62, 16))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Lucida Grande"))
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.lbVersion.setFont(font)
        self.lbVersion.setObjectName(_fromUtf8("lbVersion"))
        self.lbNversion = QtGui.QLabel(self.TbAbout)
        self.lbNversion.setGeometry(QtCore.QRect(90, 10, 62, 16))
        self.lbNversion.setObjectName(_fromUtf8("lbNversion"))
        self.lbAutores = QtGui.QLabel(self.TbAbout)
        self.lbAutores.setGeometry(QtCore.QRect(10, 60, 62, 16))
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Lucida Grande"))
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.lbAutores.setFont(font)
        self.lbAutores.setObjectName(_fromUtf8("lbAutores"))
        self.lbLogo = QtGui.QLabel(self.TbAbout)
        self.lbLogo.setGeometry(QtCore.QRect(250, 170, 221, 61))
        self.lbLogo.setText(_fromUtf8(""))
        self.lbLogo.setPixmap(QtGui.QPixmap(_fromUtf8(":/plugins/exceltoolplugin/resources/compudev.png")))
        self.lbLogo.setObjectName(_fromUtf8("lbLogo"))
        self.tabWdgAbout.addTab(self.TbAbout, _fromUtf8(""))
        self.TbHelp = QtGui.QWidget()
        self.TbHelp.setObjectName(_fromUtf8("TbHelp"))
        self.tabWdgAbout.addTab(self.TbHelp, _fromUtf8(""))

        self.retranslateUi(AboutDialog)
        self.tabWdgAbout.setCurrentIndex(0)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), AboutDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(AboutDialog)

    def retranslateUi(self, AboutDialog):
        AboutDialog.setWindowTitle(_translate("AboutDialog", "Acerca de...", None))
        self.lbNombre.setText(_translate("AboutDialog", "Excel Tools Plugin", None))
        self.lbVersion.setText(_translate("AboutDialog", "Version:", None))
        self.lbNversion.setText(_translate("AboutDialog", "0.1", None))
        self.lbAutores.setText(_translate("AboutDialog", "Autores:", None))
        self.tabWdgAbout.setTabText(self.tabWdgAbout.indexOf(self.TbAbout), _translate("AboutDialog", "About", None))
        self.tabWdgAbout.setTabText(self.tabWdgAbout.indexOf(self.TbHelp), _translate("AboutDialog", "Help", None))

import resources_rc
