# -*- coding: utf-8 -*-
"""
/***************************************************************************
 ExcelToolPlugin
                                 A QGIS plugin
 Herramienta de tablas para importar o exportar a Excel
                              -------------------
        begin                : 2014-02-26
        copyright            : (C) 2014 by CompuDev Web & Systems
        email                : mwilchez@compudev.com.ec
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from qgis.gui import *
# Initialize Qt resources from file resources.py
import resources_rc
# Import the code for the dialog
from exceltoolplugindialog import ExcelToolPluginDialog
from aboutdialog import AboutDialog
import os.path


class ExcelToolPlugin:

    MSG_BOX_TITLE = "ExcelToolPlugin Plugin Warning"

    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value("locale/userLocale")[0:2]
        localePath = os.path.join(self.plugin_dir, 'i18n', 'exceltoolplugin_{}.qm'.format(locale))

        if os.path.exists(localePath):
            self.translator = QTranslator()
            self.translator.load(localePath)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

    def initGui(self):
        # Create action that will start plugin configuration
        self.action = QAction(
            QIcon(":/plugins/exceltoolplugin/resources/icon.png"),
            u"Excel Tool", self.iface.mainWindow())
        # connect the action to the run method
        self.action.triggered.connect(self.run)

        # Add toolbar button and menu item
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToMenu(u"&Excel Tools Plugin", self.action)


    def unload(self):
        # Remove the plugin menu item and icon
        self.iface.removePluginMenu(u"&Excel Tools Plugin", self.action)
        self.iface.removeToolBarIcon(self.action)

    # run method that performs all the real work
    def run(self):
        # Create the dialog (after translation) and keep reference
        self.dlg = ExcelToolPluginDialog()
        self.loadLayer()
        # Show about Dialog
        QObject.connect(self.dlg.btnHelp, SIGNAL("clicked()"),
                        self.helpAbout)
        QObject.connect(self.dlg.btnCerrar, SIGNAL("clicked()"),
                        self.dlg.close)
        QObject.connect(self.dlg.btnBscUbc, SIGNAL("clicked()"),
                        self.selectOutputFile)

        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result == 1:
            # do something useful (delete the line containing pass and
            # substitute with your code)
            pass

    def error(self, errorStr):
        #function to return error
        QMessageBox.warning(self.iface.mainWindow(), self.MSG_BOX_TITLE,
                            str(errorStr))
        #raise errorStr
        self.dlg.close()
        self.run()

    def helpAbout(self):
        self.About = AboutDialog()
        self.About.show()

    #Load Layer in combobox
    def loadLayer(self):
        # Cargar capas en lista
        self.layers = []
        self.mapCanvas = self.iface.mapCanvas()
        layers = self.iface.activeLayer()
        if layers == None:
            QMessageBox.warning(self.iface.mainWindow(), self.MSG_BOX_TITLE,
            ("No active layer found\n" "Please make one or more layer " \
            "active\n" "Beware of layers sizes for export"), QMessageBox.Ok,
            QMessageBox.Ok)
            self.dlg.close()
            return
        #Checks vector type and populates the layer list view in opposite
        #order for the correct visualization on OL
        for i in range(self.mapCanvas.layerCount()-1, -1, -1):
            # define actual layer
            layer = self.mapCanvas.layer(i)
            #check if is a vector
            if layer.type() == layer.VectorLayer:
                self.layers.append(layer)
                source = '{name}'.format(name=layer.name())
                self.dlg.cbxTabla.addItem(source)
        #Set up the default map extent
        Extent = self.mapCanvas.extent()
        if len(self.layers) != 0:
            mylayer = self.layers[0]
            myprovider = mylayer.dataProvider()
        else:
            QMessageBox.warning(self.iface.mainWindow(), self.MSG_BOX_TITLE,
            ("No active layer found\n" "Please make one or more layer " \
            "active\n" "Beware of layers sizes for export"), QMessageBox.Ok,
            QMessageBox.Ok)
            self.dlg.close()
            return

    def selectOutputFile(self):
        global mydir
        ext = ''
        mydir = QFileDialog.getExistingDirectory(None, "Seleccione directorio de destino", "")
        if os.access(mydir, os.W_OK):
            if self.dlg.cbxFormat.currentText() == 'Excel':
                ext = ".xls"
            else:
                ext = ".cvs"
            self.dlg.txtUbicacion.setText(mydir + "/" + self.dlg.cbxTabla.currentText() + ext)
            return
        else:
            self.error("No es posible escribir en el directorio '%s'" % mydir)