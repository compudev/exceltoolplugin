# -*- coding: utf-8 -*-
"""
/***************************************************************************
 ExcelToolPlugin
                                 A QGIS plugin
 Herramienta de tablas para importar o exportar a Excel
                             -------------------
        begin                : 2014-02-26
        copyright            : (C) 2014 by CompuDev Web & Systems
        email                : mwilchez@compudev.com.ec
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""

def classFactory(iface):
    # load ExcelToolPlugin class from file ExcelToolPlugin
    from exceltoolplugin import ExcelToolPlugin
    return ExcelToolPlugin(iface)
