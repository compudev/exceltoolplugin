# -*- coding: utf-8 -*-
"""
/***************************************************************************
 ExcelToolPluginDialog
                                 A QGIS plugin
 Herramienta de tablas para importar o exportar a Excel
                             -------------------
        begin                : 2014-02-26
        copyright            : (C) 2014 by CompuDev Web & Systems
        email                : mwilchez@compudev.com.ec
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt4 import QtCore, QtGui
from ui_exceltoolplugin import Ui_ExcelToolPlugin
# create the dialog for zoom to point


class ExcelToolPluginDialog(QtGui.QDialog, Ui_ExcelToolPlugin):
    def __init__(self):
        QtGui.QDialog.__init__(self)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)
